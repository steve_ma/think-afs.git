<?php
use stevema\afs\AfsManage;
if (!function_exists('afs')) {
    function afs(): AfsManage
    {
        if(function_exists('app')) {
            return app('afs');
        }
        return new AfsManage();
    }
}
if (!function_exists('afs_check_scene')) {
    /**
     * @throws \stevema\afs\AfsException
     */
    function afs_check_scene($scene): bool
    {
        $afs = afs();
        return $afs->checkScene($scene);
    }
}
if (!function_exists('afs_create_token')) {
    /**
     * @throws \stevema\afs\AfsException
     */
    function afs_create_token($scene, $sessionId):string
    {
        $afs = afs();
        return $afs->createToken($scene, $sessionId);
    }
}
if (!function_exists('afs_check_token')) {
    function afs_check_token($scene, $sessionId, $token):bool
    {
        $afs = afs();
        return $afs->checkToken($scene, $sessionId, $token);
    }
}
if (!function_exists('afs_error_message')) {
    function afs_error_message():string
    {
        $afs = afs();
        return $afs->getErrorMessage();
    }
}