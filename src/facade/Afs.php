<?php
namespace stevema\afs\facade;

use think\Facade;

/**
 * Class Captcha
 * @package think\captcha\facade
 * @mixin \stevema\afs\AfsManage
 */
class Afs extends Facade
{
    protected static function getFacadeClass()
    {
        return \stevema\afs\AfsManage::class;
    }
}